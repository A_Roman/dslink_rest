import "dart:async";
import "dart:io";

import 'package:dslink_rest/src/helper.dart' as helper;
import "package:dslink/dslink.dart";


/// [DONE] Removed existing node from structure
Map<String,dynamic> remove(String pathOfParent, LinkProvider link) {

  bool response = false;

  try {

    link.removeNode(pathOfParent);
    link.save();

    response = true;

  } catch (e) {

    return {
      'succeeded' : response,
    };

  }

  return {
    'succeeded' : response,
  };
}

/// [###] Aadd Server Profile Action from UN / PW and whatnot.....
Future<Map<String, dynamic>> addServer(String pathOfParent, Map<String, dynamic> params, LinkProvider link) async {

  bool succeeded = false;

  String name = params["name"];
  int port = params["port"] is String ? int.parse(params["port"]) : params["port"];
  String pwd = params["password"];
  String user = params["username"];

  try {
    var server = await ServerSocket.bind(InternetAddress.ANY_IP_V4, port);
    await server.close();
  } catch (e) {
    return {
    "message": "Failed to bind to port: ${e}"
    };
  }

  link.addNode("/${name}", {
    r"$is": "server",
    r"$server_port": port,
    r"$$server_password": pwd,
    r"$$server_username": user,

    "Remove": {
      r"$is": "remove",
      r"$invokable": "write"
    }

  });

  succeeded = true;

  return {
    "succeeded": succeeded
  };

}