import "dart:async";
import "dart:convert";
import "dart:io";

import "package:dslink/dslink.dart";

String whatAmi(RemoteNode remoteNode) {

  String whatIs = remoteNode.getConfig(r'$is');
  bool invokable = remoteNode.configs.containsKey(r'$invokable');
  bool valued = remoteNode.configs.containsKey(r"$type");

  String iAm;
  if (whatIs == "dsa/broker") {
    iAm = 'Broker';

  } else if (whatIs == "dsa/link" || whatIs == "DSLink") {
    iAm = 'Link';

  } else if (invokable) {
    iAm = 'Action';

  } else if (valued) {
    iAm = 'Point';

  }else {
    iAm = 'Device';
  }

  return iAm;
}

Future<dynamic> readJSONData(HttpRequest request) async {
  var content = await request.transform(UTF8.decoder).join();
  return JSON.decode(content);
}

Future<Map<String, dynamic>> getCurrentNodeValue(RemoteNode rNodeCurrent, LinkProvider link) async {

  String path = rNodeCurrent.remotePath;

  Map<String, dynamic> valueInfo = {
    'Name'      :   rNodeCurrent.name,
    'Value'     :   null,
    'Type'      :   rNodeCurrent.get(r'$type'),
    'TimeStamp' :   null
  };

  ValueUpdate valueUpdate = await link.requester.getNodeValue(path)
      .timeout(const Duration(seconds: 1), onTimeout: () => null);

  if (valueUpdate != null) {
    valueInfo['Value']  = valueUpdate.value;
    valueInfo['TimeStamp'] = valueUpdate.timestamp.toUtc().toString();
  }
  return valueInfo;


}

