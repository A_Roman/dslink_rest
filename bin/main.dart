import "dart:async";
import "dart:convert";
import "dart:io";
import "dart:typed_data";

import "package:dslink/dslink.dart";
import "package:dslink/nodes.dart";
import "package:dslink/utils.dart";

import "package:http_multi_server/http_multi_server.dart";
import 'package:dslink_rest/src/helper.dart' as helper;

//import "package:crypto/crypto.dart";

///// Setting up variable before starting anything /////

LinkProvider link;

// underscore locks the variable
JsonEncoder jsonUglyEncoder = const JsonEncoder();
// create and encoder with tab indented
JsonEncoder jsonEncoder = const JsonEncoder.withIndent("  ");
// converts object to JSON string
String toJSON(input) => jsonEncoder.convert(input);

// load template html method with a variable name as string input
String loadTemplateFile(String name) {
  return new File("res/${name}.mustache").readAsStringSync();
}

// set read files as string variables to be used as inputs
String valuePageHtml = loadTemplateFile("value_page");
String directoryListPageHtml = loadTemplateFile("directory_list");
//
//// create a function that compiles the html
//Function valuePageTemplate = compile(valuePageHtml);
//Function directoryListPageTemplate = compile(directoryListPageHtml);

// now start actual code
launchServer(bool local, int port, String pwd, String user, ServerNodeChild serverNodeChild) async {
  // Default username as dsa if username is empty when creating server
  if (user == null || user.isEmpty) {
    user = "dsa";
  }

  // Creates an empty list of servers, loopback uses local address
  List<HttpServer> servers = <HttpServer>[];
  InternetAddress ipv4 = local ?
  InternetAddress.LOOPBACK_IP_V4 :
  InternetAddress.ANY_IP_V4;
  InternetAddress ipv6 = local ?
  InternetAddress.LOOPBACK_IP_V6 :
  InternetAddress.ANY_IP_V6;



  // try to add new servers when prompted
  try {
    servers.add(await HttpServer.bind(ipv4, port));
  } catch(e) {}

  try {
    servers.add(await HttpServer.bind(ipv6, port));
  } catch(e) {}

  // Create a new multiServer
  HttpMultiServer server = new HttpMultiServer(servers);

  handleRequest(HttpRequest request) async {
    if (pwd != null && pwd.isNotEmpty) {
      String expect = BASE64.encode(UTF8.encode("${user}:${pwd}"));

      var found = request.headers.value("Authorization");

      if (found != "Basic ${expect}") {
        request.response.headers.set(
            "WWW-Authenticate", 'Basic realm="DSA Rest Link"'
        );
        request.response.statusCode = 401;
        request.response.close();
        return;
      }
    }

    HttpResponse response = request.response;

    Uri uri = request.uri;
    String method = request.method;
    String ourPath = Uri.decodeComponent(uri.normalizePath().path);

    if (ourPath != "/" && ourPath.endsWith("/")) {
      ourPath = ourPath.substring(0, ourPath.length - 1);
    }

    String hostPath = "${serverNodeChild.path}${ourPath}";
    if (hostPath != "/" && hostPath.endsWith("/")) {
      hostPath = hostPath.substring(0, hostPath.length - 1);
    }

    response.headers.set("Cache-Control", "no-cache, no-store, must-revalidate");
    response.headers.set("Pragma", "no-cache");
    response.headers.set("Expires", "0");

    if (method == "OPTIONS") {
      response.headers.set("Access-Control-Allow-Origin", "*");
      response.headers.set("Access-Control-Allow-Methods", "GET, PUT, POST, PATCH, DELETE");
      response.writeln();
      response.close();
      return;
    }

    if (ourPath == "/favicon.ico") {
      response.statusCode = HttpStatus.NOT_FOUND;
      response.writeln("Not Found.");
      response.close();
      return;
    }

    // splits the target node into steps
    var steps = ourPath.split("/");

    // The following wont allow access to broker config nodes and its children
    var notAllowed = ["sys", "upstream", "defs", "data", "System"];

    for (String step in steps) {
      if(notAllowed.contains(step)) {
        response.statusCode = HttpStatus.NOT_FOUND;
        response.writeln("Not Allowed.");
        response.close();
        return;
      }
    };

    if (ourPath == "/index.html") {
      ourPath = "/.html";
    }


    Path currentPath = new Path(hostPath);

    Future<Map> getRemoteNodeMap(RemoteNode currentNode, {Uri uri}) async {
      if (currentNode == null) {
        return {
          "error": "No Such Node"
        };
      }

      String iAm = helper.whatAmi(currentNode);

      bool actionHeader = request.headers['action'] != null;

      String parentBroker = null;

      List<String> brokenPath = ourPath.split("/");
      int brokerNameIndex = brokenPath.lastIndexOf("downstream");

      try{
        if (brokenPath[brokerNameIndex-1] != '') {
          parentBroker = brokenPath[brokerNameIndex-1];
        }
      }
      catch(e){
        parentBroker = null;
      }

      // gives info on the current node
      var currentPath = new Path(currentNode.remotePath);
      Map<String,dynamic> map = {
        "name": currentPath.name,
        "path": ourPath,
        "parentBroker": parentBroker // return null if on parent broker
      };

      if (currentNode.getConfig(r'$disconnectedTs') != null) {
        map.putIfAbsent('isDown', () => true);
      }

      if (currentPath.name != 'downstream') {
//        map['is'] =  iAm;
      }

      // Creates respective lists
      Map<String, dynamic> brokerMap = new Map<String, dynamic>();
      Map<String, dynamic> linkMap = new Map<String, dynamic>();
      Map<String, dynamic> deviceMap = new Map<String, dynamic>();
      Map<String, dynamic> pointMap = new Map<String, dynamic>();
      List<String> actionList = [];

      if (iAm == 'Point') {
        Map<String, dynamic> valueInfo = await helper.getCurrentNodeValue(currentNode, link);
        map['value'] = valueInfo['Value'];
        map['type'] = valueInfo['Type'];
        map['timestamp'] = valueInfo['TimeStamp'];

      }

      if (iAm == 'Action') {
        map.addAll({'parameters' : currentNode.get(r'$params')});
        map.addAll({'returns' : currentNode.get(r'$columns')});
      }

//       this will get and map the values
      if(iAm != 'Action') {
        for (String key in currentNode.children.keys) {
          Node rNodeChild = currentNode.children[key];
          String childIs = helper.whatAmi(rNodeChild);

          String subPath = ('${currentPath.path}/$key');

          if (ourPath == "/") {
            subPath = subPath.substring(1);
          }

          List<String> emptyDSLinks = ['visualizer', 'CustomREST', 'Referencer', 'REST', 'System'];
          if (emptyDSLinks.contains(key.split('-')[0])) {
            continue;
          } else if (childIs == 'Broker') {
            brokerMap.putIfAbsent(key, () => '${subPath}/downstream' );
          } else if (childIs == 'Link') {
            linkMap.putIfAbsent(key, () => '${subPath}');
          } else if (childIs == 'Device') {
            deviceMap.putIfAbsent(key, () => '${subPath}');
          } else if (childIs == 'Point') {
            Map<String, dynamic> childValueInfo = await helper.getCurrentNodeValue(rNodeChild, link);
            pointMap.putIfAbsent(key, () => childValueInfo['Value']);
          } else if (childIs == 'Action') {
            actionList.add(key);
          }
        }
      }

      // Display the nodes as expected
//      if (brokerList.isNotEmpty) {
      map['brokers'] = brokerMap;
//      }
//
//      if (linkList.isNotEmpty) {
      map['links'] = linkMap;
//      }

//      if (linkList.isNotEmpty) {
//      deviceList.addAll(linkList);
//      }

//      if (deviceList.isNotEmpty) {
      map['devices'] = deviceMap;
//      }

//      if (pointList.isNotEmpty) {
      map['points'] = pointMap;
//      }

//      if (actionList.isNotEmpty) {
      map['actions'] = actionList;
//      }

      if (actionHeader) {
        if (request.headers['action'].first == 'moreInfo') {
          map.addAll(currentNode.attributes);
        }
      }

      return map;
    }


    // declares get node map
    Map getNodeMap(SimpleNode currentNode, {Uri uri}) {
      if (currentNode == null) {
        return {
          "error": "No Such Node"
        };
      }

      if (currentNode is! RestNode && currentNode is! ServerNodeChild) {
        return {
          "error": "No Such Node"
        };
      }

      var currentPath = new Path(currentNode.path);
      var map = {
        "?name": currentPath.name,
        "?path": "/" + hostPath.split("/").skip(2).join("/")
      };

      map.addAll(currentNode.configs);
      map.addAll(currentNode.attributes);

      for (var key in currentNode.children.keys) {
        var child = currentNode.children[key];

        if (child is! RestNode) {
          continue;
        }

        var x = new Path(child.path);
        map[key] = {
          "?name": x.name,
          "?path": "/" + x.path.split("/").skip(2).join("/")
        }..addAll(child.getSimpleMap());
      }

      if (currentNode.lastValueUpdate != null && currentNode.configs.containsKey(r"$type")) {
        map["?value"] = currentNode.lastValueUpdate.value;
        map["?value_timestamp"] = currentNode.lastValueUpdate.ts;
      }

      map.keys
          .where((k) => k.toString().startsWith(r"$$"))
          .toList()
          .forEach(map.remove);

      return map;
    }

    // Starts handling the GET commands

    if (method == "GET") {
      if (!serverNodeChild.isDataHost) {
//        var isHtml = false;
//        if (ourPath.endsWith(".html")) {
//          ourPath = ourPath.substring(0, ourPath.length - 5);
//          isHtml = true;
//        }

        var currentPath = new Path(ourPath);
        if (!currentPath.valid) {
          response.statusCode = HttpStatus.BAD_REQUEST;
          response.writeln(toJSON({
            "error": "Invalid Path: ${currentPath.path}"
          }));
          response.close();
          return;
        }

        var node = await link.requester.getRemoteNode(currentPath.path)
            .timeout(const Duration(seconds: 15), onTimeout: () => null);

        // if node not found (null) and node has no chilren && is just plain node.. || (node.children.keys.isEmpty && node.get(r'$is') == 'node' ).
        if (node == null) {
          response.headers.contentType = ContentType.JSON;
          response.writeln(toJSON({
            "error": "Node not found."
          }));
          response.close();
          return;
        }

        var json = await getRemoteNodeMap(node, uri: uri);

        var isImage = false;

        if (json[r"$binaryType"] == "image") {
          isImage = true;
        }

//        if (json["@filePath"] is String) {
//          var mt = lookupMimeType(json["@filePath"]);
//          if (mt is String && mt.contains("image")) {
//            isImage = true;
//          }
//        }

        //if request is for an html
//        if (isHtml) {
//          response.headers.contentType = ContentType.HTML;
//          if (json[r"$type"] != null) {
//            response.writeln(valuePageTemplate({
//              "name": json.containsKey(r"$name") ? json[r"$name"] : json["?name"],
//              "path": json["?path"],
//              "editor": json[r"$editor"],
//              "binaryType": json[r"$binaryType"],
//              "isImage": isImage,
//              "isNotImage": !isImage
//            }));
//          } else {
//            response.writeln(directoryListPageTemplate({
//              "name": json.containsKey(r"$name") ? json[r"$name"] : json["?name"],
//              "path": json["?path"],
//              "url": json["?url"],
//              "parent": currentPath.parentPath + ".html",
//              "children": json.keys.where((String x) => x.isNotEmpty && !(
//                  (const ["@", "!", "?", r"$"]).contains(x[0]))).map((x) {
//                var currentNode = json[x];
//                return {
//                  "name": currentNode["?name"],
//                  "url": currentNode["?url"],
//                  "path": currentNode["?path"],
//                  "isValue": currentNode[r"$type"] != null,
//                  "isAction": currentNode[r"$invokable"] != null,
//                  "isNode": currentNode[r"$invokable"] == null && currentNode[r"$type"] == null
//                };
//              }).toList()
//            }));
//          }
//          response.close();
//          return;
//        }

        //if query calls for val
        if (uri.queryParameters.containsKey("val") || uri.queryParameters.containsKey("value")) {
          String etag = json["?value_timestamp"];

          response.headers.set("ETag", etag);
          response.headers.set("Cache-Control", "public, max-age=31536000");
          json = json["?value"];

          if (request.headers["If-None-Match"] != null && request.headers["If-None-Match"].isNotEmpty) {
            var lastEtag = request.headers.value("If-None-Match");
            if (etag == lastEtag) {
              response.statusCode = HttpStatus.NOT_MODIFIED;
              response.close();
              return;
            }
          }

          if (json is ByteData) {
            var byteList = json.buffer.asUint8List(
                json.offsetInBytes,
                json.lengthInBytes
            );

//            if (request.uri.queryParameters.containsKey("detectType")) {
//              var result = lookupMimeType("binary", headerBytes: byteList);
//              if (result != null) {
//                response.headers.contentType = ContentType.parse(result);
//              } else {
//                response.headers.contentType = isImage ? ContentType.parse("image/jpeg") : ContentType.BINARY;
//              }
//            } else {
//              response.headers.contentType = isImage ? ContentType.parse("image/jpeg") : ContentType.BINARY;
//            }

            response.add(byteList);
          } else if (json is Map || json is List) {
            response.headers.contentType = ContentType.JSON;
            response.write(toJSON(json));
          } else {
            response.write(json);
          }
          response.close();
          return;
          // calls for a subscription
        } /* else if (uri.queryParameters.containsKey("watch") ||
            uri.queryParameters.containsKey("subscribe")) {
          if (!(await WebSocketTransformer.isUpgradeRequest(request))) {
            request.response.statusCode = HttpStatus.BAD_REQUEST;
            request.response.writeln("Bad Request: Expected WebSocket Upgrade.");
            request.response.close();
            return;
          }

          var socket = await WebSocketTransformer.upgrade(request);

          ReqSubscribeListener sub;
          Function onValueUpdate;
          onValueUpdate = (ValueUpdate update) {
            if (socket.closeCode != null) {
              return;
            }

            var value = update.value;
            var ts = update.ts;
            var isBinary = value is ByteData || value is Uint8List;

            if (value is ByteData) {
              value = value.buffer.asUint8List();
            }

            if (value is Uint8List) {
              value = BASE64.encode(value);
            }

            var msg = {
              "value": value,
              "timestamp": ts
            };

            if (isBinary) {
              msg["bin"] = true;
            }

            socket.add(jsonUglyEncoder.convert(msg));
          };
          sub = link.requester.subscribe(ourPath, onValueUpdate);
          socket.done.then((_) {
            sub.cancel();
          });
          return;
        }*/ else {
          response.headers.contentType = ContentType.JSON;
          response.writeln(toJSON(json));
        }
        response.close();
        return;
      }

      SimpleNode currentNode = link.getNode(hostPath);

      if (!(link.provider as SimpleNodeProvider).hasNode(hostPath)) {
        response.statusCode = HttpStatus.NOT_FOUND;
        response.headers.contentType = ContentType.JSON;
        response.writeln(toJSON({
          "error": "No Such Node"
        }));
        response.close();
        return;
      }

      var map = getNodeMap(currentNode);

      if (uri.queryParameters.containsKey("val") ||
          uri.queryParameters.containsKey("value")) {
        map = map["?value"];
        if (map is ByteData) {
          response.headers.contentType = ContentType.BINARY;
          response.add(map.buffer.asUint8List());
        } else if (map is Map || map is List) {
          response.headers.contentType = ContentType.JSON;
          response.write(toJSON(map));
        } else {
          response.write(map);
        }
      } else if (uri.queryParameters.containsKey("watch") ||
          uri.queryParameters.containsKey("subscribe")) {
        if (!(await WebSocketTransformer.isUpgradeRequest(request))) {
          request.response.statusCode = HttpStatus.BAD_REQUEST;
          request.response.writeln("Bad Request: Expected WebSocket Upgrade.");
          request.response.close();
          return;
        }

        var socket = await WebSocketTransformer.upgrade(request);

        RespSubscribeListener sub;
        sub = currentNode.subscribe((ValueUpdate update) {
          if (socket.closeCode != null) {
            if (sub != null) {
              sub.cancel();
            }
            return;
          }

          socket.add(jsonUglyEncoder.convert({
            "value": update.value,
            "timestamp": update.ts
          }));
        });

        socket.done.then((_) {
          sub.cancel();
          sub = null;
        });
        return;
      } else {
        response.headers.contentType = ContentType.JSON;
        response.writeln(toJSON(map));
      }
      response.close();
      return;
    } else if (method == "POST") {
      var json = await readJSONData(request);

      Map<String, dynamic> map = {};

      if(!serverNodeChild.isDataHost) {

        // if header is there
        if (request.headers.value('action')!=null) {

          // if header is incorrect
          if (request.headers.value('action') == 'invoke'){  //request.headers.value('action') == 'invoke' disbaled to test deserialization

            // get the node 'action' node
            var node = await link.requester.getRemoteNode(ourPath)
                .timeout(const Duration(seconds: 2), onTimeout: () => null);

            if (node == null || (node.children.keys.isEmpty && node.get(r'$is') == 'node' )) {
              response.headers.contentType = ContentType.JSON;
              response.writeln(toJSON({
                "error": "Node not found."
              }));
              response.close();
              return;
            }

            if (node.configs[r"$invokable"] == null) {
              response.statusCode = HttpStatus.NOT_IMPLEMENTED;
              response.headers.contentType = ContentType.JSON;
              response.writeln(toJSON({
                "error": "Node is not invokable"
              }));
              response.close();
              return;
            }

            Map<String, dynamic> paramsToPass = {};

            var paramsFromSourceIHM = node.get(r'$params');

            bool containsParams = true;

            bool jsonEmpty = json.isEmpty;
            bool paramsEmpty = paramsFromSourceIHM == null;

            // my source json in comparison to source params
            if (jsonEmpty == paramsEmpty) {

              if (!jsonEmpty) {
                Map<String, dynamic> paramsSource = {};

                for (var param in paramsFromSourceIHM) {
                  paramsSource[param['name']] = param['type'];
                }

                for (String source in json.keys) {
                  if (!paramsSource.keys.contains(source)) {
                    containsParams = false;
                  } else {
                    paramsToPass[source] = json[source];
                  };
                }
              }

            } else {
              containsParams = false;
            }

            if (containsParams) {

              var stream = link.requester.invoke(ourPath, paramsToPass);
              List<RequesterInvokeUpdate> invokeUpdates = [];

              StreamSubscription sub;
              sub = stream.listen((RequesterInvokeUpdate invokeUpdate) {
                invokeUpdates.add(invokeUpdate);
              });

              var future = sub.asFuture().timeout(
                  new Duration(seconds: 45), onTimeout: () {
                sub.cancel();
              });

              await future;

              // if list is empty dont add to map, note the for in for in for
              // thats because of the construction of updates... where updates
              // can live in updates and those updates is a list of Maps...

              Map<String,dynamic> tempResponseMap = {};
              if (!invokeUpdates.isEmpty){
                for (var invokeUpdate in invokeUpdates) {
                  int i = 0;
                  for (var column in invokeUpdate.columns) {

                    String reponseName = column.name;

                    String updateResponseType = invokeUpdate.updates[0].runtimeType.toString();

                    var responseUpdate;
                    if (updateResponseType == '_InternalLinkedHashMap') {

                      Map<String,String> tempUpdateResponse = invokeUpdate.updates[0];
                      responseUpdate = tempUpdateResponse.values.elementAt(i);



                    } else if (updateResponseType == 'List') {

                      List tempUpdateResponse = invokeUpdate.updates[0];
                      tempUpdateResponse.forEach( (String response) {
                        responseUpdate = response;
                      });

                    }

                    tempResponseMap[reponseName] = responseUpdate;

                    i++;
                  }
                }
              }
              map.addAll(tempResponseMap);
            } else {
              map['Error'] = 'JSON payload and node parameters do not match!';
            }
          } else {
            map['Error'] = 'incorrect {action} header value';
          }
        } else {
          map['Error'] = 'missing {action} header key';
        }
      }

      Map<String, dynamic> failure = {
        'succeeded' : false
      };

      if (map == failure) {
        response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
      } else {
        response.statusCode = HttpStatus.OK;
      }

      response.headers.contentType = ContentType.JSON;
      response.writeln(toJSON(map));
      response.close();
      changed = true;
      return;
    } else {
      response.writeln(toJSON({
        "error": "Request not supported"
      }));
    };

    response.headers.contentType = ContentType.JSON;
    response.statusCode = HttpStatus.BAD_REQUEST;
    response.writeln(toJSON({
      "error": "Bad Request"
    }));
    response.close();
  }


  server.listen((request) async {
    try {
      await handleRequest(request);
    } catch (e) {
      try {
        request.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
      } catch (e) {}
      request.response.writeln("Internal Server Error:");
      request.response.writeln(e);
      request.response.close();
    }
  });

  return server;
}

Future<dynamic> readJSONData(HttpRequest request) async {
  var content = await request.transform(UTF8.decoder).join();
  return JSON.decode(content);
}


main(List<String> args) async {
  link = new LinkProvider(args, "REST-", profiles: {
    "addServer": (String path) => new SimpleActionNode(path,
            (Map<String, dynamic> params) async {
          int port = params["port"] is String  ?
          int.parse(params["port"]) :
          params["port"];
          bool local = params["local"];
          String type = params["type"];
          String pwd = params["password"];
          String user = params["username"];
          if (local == null) local = false;

          try {
            var server = await ServerSocket.bind(InternetAddress.ANY_IP_V4, port);
            await server.close();
          } catch (e) {
            return {
              "message": "Failed to bind to port: ${e}"
            };
          }

          link.addNode("/${params["name"]}", {
            r"$is": "server",
            r"$server_port": port,
            r"$server_local": local,
            r"$server_type": type,
            r"$$server_password": pwd,
            r"$$server_username": user,
            "Remove": {
              r"$is": "remove",
              r"$invokable": "write"
            }
          });
          changed = true;
          return {
            "message": "Success!"
          };
        }),
    "server": (String path) {
      return new ServerNodeChild(path);
    },
    "rest": (String path) {
      return new RestNode(path);
    },
    "create": (String path) {
      return new SimpleActionNode(path, (Map<String, dynamic> params) {
        var name = params["name"];

        var parent = new Path(path).parent;
        link.addNode("${parent.path}/${name}", {
          r"$is": "rest"
        });
        changed = true;
      });
    },
    "createMetric": (String path) {
      return new SimpleActionNode(path, (Map<String, dynamic> params) {
        var name = params["name"];
        var editor = params["editor"];
        var type = params["type"];

        var parent = new Path(path).parent;
        var node = link.addNode("${parent.path}/${name}", {
          r"$is": "rest",
          r"$type": type,
          r"$writable": "write"
        });

        if (editor != null && editor.isNotEmpty) {
          node.configs[r"$editor"] = editor;
        }

        changed = true;
      });
    },
    "remove": (String path) => new DeleteActionNode.forParent(
        path,
        link.provider as MutableNodeProvider,
        onDelete: link.save
    )
  },encodePrettyJson: true
      , autoInitialize: false
      , isRequester: true
      , isResponder: true);

  var nodes = {
    "addServer": {
      r"$name": "Add Server",
      r"$invokable": "write",
      r"$params": [
        {
          "name": "name",
          "type": "string",
          "placeholder": "MyServer"
        },
        {
          "name": "local",
          "type": "bool",
          "description": "Bind to Local Interface",
          "default": false
        },
        {
          "name": "port",
          "type": "int",
          "default": 8020
        },
        {
          "name": "type",
          "type": "enum[Data Host,Data Client]",
          "default": "Data Host",
          "description": "Data Type"
        },
        {
          "name": "username",
          "type": "string",
          "placeholder": "Optional Username"
        },
        {
          "name": "password",
          "type": "string",
          "editor": "password",
          "placeholder": "Optional Password"
        },

      ],
      r"$result": "values",
      r"$columns": [
        {
          "name": "message",
          "type": "string"
        }
      ],
      r"$is": "addServer"
    }
  };

  link.init();

  for (var k in nodes.keys) {
    link.addNode("/${k}", nodes[k]);
  }

  link.connect();

  timer = Scheduler.every(Interval.ONE_SECOND, () async {
    if (changed) {
      changed = false;
      await link.saveAsync();
    }
  });
}

bool changed = false;
Timer timer;

class RestNode extends SimpleNode {
  RestNode(String path) : super(path);

  @override
  onCreated() {
    link.addNode("${path}/createNode", {
      r"$name": "Create Node",
      r"$is": "create",
      r"$invokable": "write",
      r"$result": "values",
      r"$params": [
        {
          "name": "name",
          "type": "string"
        }
      ]
    });

    link.addNode("${path}/createValue", CREATE_VALUE);

    link.addNode("${path}/remove", {
      r"$name": "Remove Node",
      r"$is": "remove",
      r"$invokable": "write"
    });
  }

  @override
  onSetValue(Object val) {
    if (configs[r"$type"] == "map" && val is String) {
      try {
        var json = JSON.decode(val);
        updateValue(json);
        return true;
      } catch (e) {
        return super.onSetValue(val);
      }
    } else {
      return super.onSetValue(val);
    }
  }

  @override
  updateValue(Object update, {bool force: false}) {
    super.updateValue(update, force: force);
    changed = true;
  }

  @override
  onRemoving() {
    changed = true;
  }
}

// Create a value by mapping it into existing node
final Map<String, dynamic> CREATE_VALUE = {
  r"$name": "Create Value",
  r"$is": "createMetric",
  r"$invokable": "write",
  r"$result": "values",
  r"$params": [
    {
      "name": "name",
      "type": "string"
    },
    {
      "name": "type",
      "type": "enum",
      "editor": buildEnumType([
        "string",
        "number",
        "bool",
        "color",
        "gradient",
        "fill",
        "array",
        "map"
      ])
    },
    {
      "name": "editor",
      "type": "enum",
      "editor": buildEnumType([
        "none",
        "textarea",
        "password",
        "daterange",
        "date"
      ]),
      "default": "none"
    }
  ],
  r"$columns": []
};

// Create ServerNodeChild and defines its creation
class ServerNodeChild extends SimpleNode {
  HttpServer server;

  ServerNodeChild(String path) : super(path);

  @override
  onCreated() async {
    var port = configs[r"$server_port"];
    var local = configs[r"$server_local"];
    var type = configs[r"$server_type"];

    // using the $$ means for a client to see it they need config permission
    var user = configs[r"$$server_username"];
    var pwd = configs[r"$$server_password"];

    // default to be shown
    if (local == null) local = false;
    if (type == null) type = "Data Host";
    configs[r"$server_local"] = local;
    configs[r"server_type"] = type;

    // creates a new server
    server = await launchServer(local, port, pwd, user, this);

    if (type == "Data Host") {
      link.addNode("${path}/createNode", {
        r"$name": "Create Node",
        r"$is": "create",
        r"$invokable": "write",
        r"$result": "values",
        r"$params": [
          {
            "name": "name",
            "type": "string"
          }
        ]
      });

      link.addNode("${path}/createValue", CREATE_VALUE);
    }
  }

  // If server is Data Host then it will allow creating of nodes under it
  bool get isDataHost => configs[r"$server_type"] == "Data Host";

  @override
  onRemoving() async {
    if (server != null) {
      await server.close(force: true);
      server = null;
    }
  }
}